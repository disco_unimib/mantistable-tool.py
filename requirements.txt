djongo[json]==1.2.38
django==2.2.8
jsonfield==2.1.1
sqlparse==0.2.4
celery[redis]
psycopg2-binary
nltk
isbnlib
channels
django-jsoneditor
channels_redis
sparqlwrapper
python-dateutil
ftfy
Unidecode
whitenoise
networkx
rdflib-jsonld
inflect
requests
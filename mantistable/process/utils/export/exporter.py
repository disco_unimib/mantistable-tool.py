from abc import abstractmethod


class Exporter:
    @abstractmethod
    def export(self):
        raise NotImplemented()

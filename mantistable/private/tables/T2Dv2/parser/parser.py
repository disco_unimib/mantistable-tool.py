import os
import json
import ftfy
import unidecode

def main():
    #parser_T2D()
    #stats_t2d()
    generate_groundtruth_t2d()

def parser_T2D():

    def extract_header(cols, header, has_header):
        if has_header:
            for i, col in enumerate(cols):
                header_attribute = col[0]
                if header_attribute == '':
                    header_attribute = f'col {i+1}'
                if header_attribute in header:
                    header_attribute += ' ' + str(i+1)
                data = header_attribute
                data = ftfy.fixes.remove_control_chars(data)
                data = ftfy.fixes.decode_escapes(data)
                data = ftfy.fix_text(data)
                data = unidecode.unidecode(data)
                header_attribute = data        
                header.append(header_attribute)
        else:
            for i in range(0, len(cols)):
                header.append(f'col {i+1}')        
    
    def get_json_table(cols, header, has_header):
        table = []
        start = 0
        if has_header:
            start = 1      
        for i in range(start, len(cols[0])):
            item = {key:cols[j][i] for j, key in enumerate(header)}
            table.append(item)    
        return table
    
    i = 0
    list_tables = json.loads(open("./t2d/t2dTables.json", "r").read())
    target_col = {}
    for table_name in list_tables:
        print(table_name)
        table_data = json.loads(open(f'./t2d/tables/{table_name}.json', "r", encoding="latin").read())
        i += 1
        target_col[table_name] = int(table_data["keyColumnIndex"])
        cols = table_data["relation"]
        header = []
        extract_header(cols, header, table_data["hasHeader"])
        json_table = get_json_table(cols, header, table_data["hasHeader"])
        write_file("./converted/", table_name, json_table, True)
        write_file("./", "Target", target_col, True)
        #break
        #print(table_name, header)  
    print("# tables ", i)


def generate_groundtruth_t2d():
    lines = open('./t2d/classes_GS.csv').read().strip().split("\n")
    groundtruth = {}
    for line in lines:
        tmp = line[1:len(line)-1]
        tmp = tmp.split('","')
        table_name = tmp[0][0:len(tmp[0])-7]
        annotation = tmp[2][28:]
        groundtruth[table_name] = annotation
    write_file("./", "groundtruth", groundtruth, True)

def write_file(path, filename, data, is_json):
    extension = ''
    if is_json:
        extension = '.json'
        data = json.dumps(data, indent=4)
    f = open(f"{path}{filename}{extension}", "w")
    f.write(data)
    f.close()

def stats_t2d():
    count_has_header = 0
    count_first_row = 0
    count_has_key_col = 0
    i = 0
    list_tables = json.loads(open("./t2d/t2dTables.json", "r").read())
    tmp1 = []
    tmp2 = []
    for table_name in list_tables:
        print(table_name)
        table_data = json.loads(open(f'./t2d/tables/{table_name}.json', "r", encoding="ISO-8859-1").read())
        if bool(table_data["hasHeader"]) == True:
            count_has_header += 1
        else:
            tmp1.append(table_name)    
        if table_data["headerPosition"] == 'FIRST_ROW':
            count_first_row += 1
        else:
            tmp2.append(table_name)
        if bool(table_data["hasKeyColumn"]) == True:
            count_has_key_col += 1
        i += 1    
        
    print("# table has header ", count_has_header)
    print("# table has header position first row ", count_first_row)
    print("# table has key col ", count_has_key_col)
    print("# tables ", i)
    print(tmp1)

if __name__ == '__main__':
    main()
